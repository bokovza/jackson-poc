package com.boko.jackson.poc.tests;

import com.boko.jackson.poc.Person;
import com.boko.jackson.poc.mapping.ClassificationDataHolder;
import com.boko.jackson.poc.mapping.ClassificationMapper;
import com.boko.jackson.poc.JsonSerde;
import com.boko.jackson.poc.classifications.CityClassificationData;
import com.boko.jackson.poc.classifications.ClassificationData;
import com.boko.jackson.poc.classifications.GenderClassificationData;
import com.boko.jackson.poc.classifications.IncomeClassificationData;
import com.boko.jackson.poc.enums.City;
import com.boko.jackson.poc.enums.Classification;
import com.boko.jackson.poc.enums.Gender;
import com.boko.jackson.poc.enums.Income;
import com.boko.jackson.poc.mapping.ClassificationMapping;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class ClassificationTest {
    private Map<Classification, List<ClassificationData>> baseMap;
    private final JsonSerde jsonSerde = new JsonSerde();
    private ClassificationMapper mapper;
    private ClassificationDataHolder holder;

    @Before
    public void initialize() {
        List<ClassificationData> blackList = Arrays.asList(
                new GenderClassificationData(Gender.Female),
                new CityClassificationData(City.BeerSheva)
        );

        List<ClassificationData> purpleList = Arrays.asList(
                new IncomeClassificationData(Income.BillGates),
                new CityClassificationData(City.Eilat)
        );

        List<ClassificationData> greenList = Arrays.asList(
                new GenderClassificationData(Gender.Male),
                new IncomeClassificationData(Income.None),
                new CityClassificationData(City.Haifa),
                new CityClassificationData(City.Other)
        );

        this.baseMap = Map.of(
                Classification.Black, blackList,
                Classification.Purple, purpleList,
                Classification.Green, greenList
        );

        final ClassificationMapping mapping = new ClassificationMapping(Classification.White, this.baseMap);
        this.mapper = new ClassificationMapper(mapping);
    }

    @Before
    public void initializeHolder() {
        holder = new ClassificationDataHolder();
        holder.setIncome(new IncomeClassificationData(Income.Average));
        holder.setGender(new GenderClassificationData(Gender.Male));
        holder.setCities(Arrays.asList(
                new CityClassificationData(City.TelAviv),
                new CityClassificationData(City.Other)
        ));
    }

    @Test
    public void testSerialization() {
        final TypeReference type = new TypeReference<Map<Classification, List<ClassificationData>>>() {
        };
        final Optional<String> json = jsonSerde.toPolymorphicJson(baseMap, type);

        Assert.assertTrue(json.isPresent());
        Assert.assertTrue(json.get().contains(ClassificationData.TYPE_PROPERTY));

        System.out.println(json.get());

        Optional<Map<Classification, List<ClassificationData>>> output = jsonSerde.fromPolymorphicJson(json.get(), type);

        Assert.assertTrue(output.isPresent());

        Map<Classification, List<ClassificationData>> mapping = output.get();
        Assert.assertEquals(this.baseMap.size(), mapping.size());
        Assert.assertTrue(this.baseMap.keySet().stream().allMatch(mapping::containsKey));

        this.baseMap.keySet().forEach(x -> {
            List<ClassificationData> data = mapping.get(x);
            Assert.assertNotNull(data);
            Assert.assertEquals(data.size(), this.baseMap.get(x).size());
        });
    }

    @Test
    public void testMapper() {
        Assert.assertEquals(Classification.Green, mapper.getClassification(new CityClassificationData(City.Haifa)));

        Assert.assertEquals(Classification.Black, mapper.getClassification(
                new CityClassificationData(City.Other),
                new CityClassificationData(City.BeerSheva),
                new IncomeClassificationData(Income.None),
                new CityClassificationData(City.TelAviv)
        ));

        Assert.assertEquals(Classification.White, mapper.getClassification(
                new CityClassificationData(City.TelAviv),
                new CityClassificationData(City.TelAviv),
                new CityClassificationData(City.TelAviv)
        ));

        Optional<ClassificationData> argClassification = mapper.getArgClassification(
                new CityClassificationData(City.Other),
                new CityClassificationData(City.BeerSheva),
                new IncomeClassificationData(Income.None),
                new CityClassificationData(City.TelAviv)
        );

        Assert.assertTrue(argClassification.isPresent());
        Assert.assertEquals(new CityClassificationData(City.BeerSheva), argClassification.get());

        Assert.assertFalse(mapper.getArgClassification().isPresent());

        System.out.println(argClassification.get().getClassificationGroupName());
    }

    @Test
    public void testHolder() {
        final Optional<String> json = jsonSerde.toJson(this.holder);

        Assert.assertTrue(json.isPresent());
        System.out.println(json.get());
    }

    @Test
    public void testPerson() {
        final Person person = new Person();
        person.setFirstName("Bar");
        person.setLastName("Bokovza");
        person.setHolder(this.holder);

        final Optional<String> json = jsonSerde.toJson(person);
        Assert.assertTrue(json.isPresent());
        System.out.println(json.get());

        Optional<Person> output = jsonSerde.fromJson(json.get(), Person.class);
        Assert.assertTrue(output.isPresent());
        Assert.assertEquals(person, output.get());
    }
}
