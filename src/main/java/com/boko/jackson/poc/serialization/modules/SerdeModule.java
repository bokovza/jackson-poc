package com.boko.jackson.poc.serialization.modules;

import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class SerdeModule<T> extends SimpleModule {
    public SerdeModule(Class<T> clazz, StdSerializer<T> serializer, StdDeserializer<T> deserializer) {
        super();

        this.addSerializer(clazz, serializer);
        this.addDeserializer(clazz, deserializer);
    }
}
