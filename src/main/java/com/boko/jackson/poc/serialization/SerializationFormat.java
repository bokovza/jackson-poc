package com.boko.jackson.poc.serialization;

public enum SerializationFormat {
    Json,
    JsonPolymorphism,
    MongoBson,
    Yaml,
    Default
}
