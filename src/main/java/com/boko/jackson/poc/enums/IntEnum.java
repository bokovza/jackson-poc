package com.boko.jackson.poc.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public interface IntEnum {
    @JsonValue
    int getValue();
}
