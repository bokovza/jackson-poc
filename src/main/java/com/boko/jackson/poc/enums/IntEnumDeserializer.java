package com.boko.jackson.poc.enums;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class IntEnumDeserializer<T extends Enum<T> & IntEnum> {
    private final List<T> values;
    private final T defaultValue;

    public List<T> getValues() {
        return values;
    }

    public T getDefaultValue() {
        return defaultValue;
    }

    public IntEnumDeserializer(List<T> values, T defaultValue) {
        this.values = values;
        this.defaultValue = defaultValue;

        ensureDeserializationFields();
    }

    public IntEnumDeserializer(T[] values, T defaultValue) {
        this.values = Arrays.asList(values);
        this.defaultValue = defaultValue;

        ensureDeserializationFields();
    }

    private void ensureDeserializationFields() {
        if(Optional.ofNullable(this.values).map(List::isEmpty).orElse(true)) {
            throw new IllegalArgumentException("Values of Int Enum Deserializer must have at least 1 value");
        }

        if(!Optional.ofNullable(this.defaultValue).isPresent()) {
            throw new IllegalArgumentException("Default Value can't be null");
        }

        if(this.values.stream().map(IntEnum::getValue).distinct().count() < this.values.size()) {
            throw new IllegalArgumentException("There are 2 enums that have the same int value");
        }
    }

    public T deserialize(String value) {
        return deserialize(Integer.valueOf(value));
    }

    private T deserialize(int value) {
        return this.values.stream()
                .filter(x -> x.getValue() == value)
                .findFirst()
                .orElse(this.defaultValue);
    }
}