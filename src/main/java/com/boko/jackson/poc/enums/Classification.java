package com.boko.jackson.poc.enums;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum Classification implements IntEnum {
    White(0),
    Yellow(1),
    Red(2),
    Blue(3),
    Pink(4),
    Brown(5),
    Azure(6),
    Green(7),
    Purple(8),
    Orange(9),
    Black(10);

    private static final IntEnumDeserializer<Classification> deserializer
            = new IntEnumDeserializer<>(Classification.values(), Classification.White);

    private int value;

    public int getValue() {
        return this.value;
    }

    Classification(int value) {
        this.value = value;
    }

    @JsonCreator
    public static Classification fromValue(String value) {
        return deserializer.deserialize(value);
    }
}
