package com.boko.jackson.poc.enums;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum Gender implements IntEnum {
    Male(1),
    Female(2),
    Queer(3),
    TransgenderToMale(4),
    TransgenderToFemale(5);

    private static final IntEnumDeserializer<Gender> deserializer
            = new IntEnumDeserializer<>(Gender.values(), Gender.Queer);

    private int value;

    public int getValue() {
        return this.value;
    }

    Gender(int value) {
        this.value = value;
    }

    @JsonCreator
    public static Gender fromValue(String value) {
        return deserializer.deserialize(value);
    }

}
