package com.boko.jackson.poc.enums;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum Income implements IntEnum {
    None(0),
    LowerThanAverage(1),
    Average(2),
    HigherThanAverage(3),
    BillGates(4);

    private static final IntEnumDeserializer<Income> deserializer
            = new IntEnumDeserializer<>(Income.values(), Income.None);

    private int value;

    public int getValue() {
        return this.value;
    }

    Income(int value) {
        this.value = value;
    }

    @JsonCreator
    public static Income fromValue(String value) {
        return deserializer.deserialize(value);
    }
}
