package com.boko.jackson.poc.enums;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum City implements IntEnum {
    TelAviv(10),
    Eilat(11),
    Haifa(12),
    Jerusalem(13),
    Hertzliyya(14),
    BeerSheva(15),
    Other(16);

    private static final IntEnumDeserializer<City> deserializer
            = new IntEnumDeserializer<>(City.values(), City.Other);

    private int value;

    public int getValue() {
        return this.value;
    }

    City(int value) {
        this.value = value;
    }

    @JsonCreator
    public static City fromValue(String value) {
        return deserializer.deserialize(value);
    }
}
