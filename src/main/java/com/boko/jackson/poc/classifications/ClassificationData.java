package com.boko.jackson.poc.classifications;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface ClassificationData {
    String TYPE_PROPERTY = "@type";

    @JsonIgnore
    String getClassificationGroupName();
}
