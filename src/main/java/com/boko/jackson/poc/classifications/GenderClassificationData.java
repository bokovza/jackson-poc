package com.boko.jackson.poc.classifications;

import com.boko.jackson.poc.enums.Gender;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public final class GenderClassificationData implements ClassificationData {
    public static final String TYPE = "gender";

    private Gender gender;

    public GenderClassificationData() {
    }

    public GenderClassificationData(Gender gender) {
        this.gender = gender;
    }

    @Override
    public String getClassificationGroupName() {
        return String.format("%s-%d", TYPE, this.gender.getValue());
    }
}
