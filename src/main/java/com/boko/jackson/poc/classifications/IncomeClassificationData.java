package com.boko.jackson.poc.classifications;

import com.boko.jackson.poc.enums.Income;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public final class IncomeClassificationData implements ClassificationData {
    public static final String TYPE = "income";

    private Income income;

    public IncomeClassificationData() {
    }

    public IncomeClassificationData(Income income) {
        this.income = income;
    }

    @Override
    public String getClassificationGroupName() {
        return String.format("%s-%d", TYPE, this.income.getValue());
    }
}
