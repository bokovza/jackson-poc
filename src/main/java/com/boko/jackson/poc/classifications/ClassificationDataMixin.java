package com.boko.jackson.poc.classifications;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = ClassificationData.TYPE_PROPERTY)
@JsonSubTypes({
        @JsonSubTypes.Type(name = CityClassificationData.TYPE, value = CityClassificationData.class),
        @JsonSubTypes.Type(name = GenderClassificationData.TYPE, value = GenderClassificationData.class),
        @JsonSubTypes.Type(name = IncomeClassificationData.TYPE, value = IncomeClassificationData.class)
})
public abstract class ClassificationDataMixin {
}
