package com.boko.jackson.poc.classifications;

import com.boko.jackson.poc.enums.City;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public final class CityClassificationData implements ClassificationData {
    public static final String TYPE = "city";

    private City city;

    public CityClassificationData() {

    }

    public CityClassificationData(City city) {
        this.city = city;
    }

    @Override
    public String getClassificationGroupName() {
        return String.format("%s-%d", TYPE, this.city.getValue());
    }
}
