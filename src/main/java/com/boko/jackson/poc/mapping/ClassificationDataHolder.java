package com.boko.jackson.poc.mapping;

import com.boko.jackson.poc.classifications.CityClassificationData;
import com.boko.jackson.poc.classifications.GenderClassificationData;
import com.boko.jackson.poc.classifications.IncomeClassificationData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class ClassificationDataHolder {
    private List<CityClassificationData> cities;
    private IncomeClassificationData income;
    private GenderClassificationData gender;
}
