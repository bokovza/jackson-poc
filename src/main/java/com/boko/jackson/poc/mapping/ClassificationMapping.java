package com.boko.jackson.poc.mapping;

import com.boko.jackson.poc.classifications.ClassificationData;
import com.boko.jackson.poc.enums.Classification;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

@Data
@EqualsAndHashCode
public class ClassificationMapping {
    private final Classification defaultClassification;
    private final Map<Classification, List<ClassificationData>> rules;
}
