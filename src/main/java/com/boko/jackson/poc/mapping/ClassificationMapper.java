package com.boko.jackson.poc.mapping;

import com.boko.jackson.poc.classifications.ClassificationData;
import com.boko.jackson.poc.enums.Classification;

import java.util.*;
import java.util.stream.Stream;

public class ClassificationMapper {
    private final ClassificationMapping mapping;
    private final Map<ClassificationData, Classification> dataToClassification;

    public ClassificationMapper(ClassificationMapping mapping) {
        this.mapping = mapping;

        final Map<ClassificationData, Classification> dataToClassification = new HashMap<>();
        mapping.getRules().keySet().forEach(classification -> Optional.ofNullable(mapping.getRules().get(classification))
                .filter(list -> !list.isEmpty())
                .ifPresent(list -> list.forEach(dataItem -> dataToClassification.put(dataItem, classification))));

        this.dataToClassification = Collections.unmodifiableMap(dataToClassification);
    }

    public Classification getClassification(ClassificationData... data) {
        return getClassification(Stream.of(data));
    }

    public Classification getClassification(Collection<ClassificationData> data) {
        return Optional.ofNullable(data)
                .map(x -> this.getClassification(x.stream()))
                .orElse(mapping.getDefaultClassification());
    }

    public Classification getClassification(Stream<ClassificationData> data) {
        return Optional.ofNullable(data)
                .map(this::getReducedClassification)
                .orElse(mapping.getDefaultClassification());
    }

    public Classification getClassification(ClassificationDataHolder holder) {
        return Optional.ofNullable(holder)
                .map(actualHolder -> Stream.concat(
                        ClassificationMapper.getStreamOfCollection(holder.getCities()),
                        Stream.of(actualHolder.getGender(), actualHolder.getIncome())
                ))
                .map(this::getClassification)
                .orElse(mapping.getDefaultClassification());
    }

    private Classification getReducedClassification(Stream<ClassificationData> stream) {
        return stream.map(x -> this.dataToClassification.getOrDefault(x, mapping.getDefaultClassification()))
                .reduce(mapping.getDefaultClassification(), this::getDominantClassification, this::getDominantClassification);
    }

    private Optional<ClassificationData> getArgReducedClassification(Stream<ClassificationData> stream) {
        return stream.map(Optional::ofNullable)
                .reduce(Optional.empty(), this::getArgDominantClassification, this::getArgDominantClassification);
    }

    private Classification getDominantClassification(Classification a, Classification b) {
        if (a == null && b == null) {
            return mapping.getDefaultClassification();
        }

        if (a == null) {
            return b;
        }

        if (b == null) {
            return a;
        }

        return a.getValue() >= b.getValue() ? a : b;
    }

    private Optional<ClassificationData> getArgDominantClassification(Optional<ClassificationData> a, Optional<ClassificationData> b) {
        final Classification classA = a.map(this::getClassification).orElse(mapping.getDefaultClassification());
        final Classification classB = b.map(this::getClassification).orElse(mapping.getDefaultClassification());
        final Classification dominantClassification = getDominantClassification(classA, classB);

        if(dominantClassification == mapping.getDefaultClassification()) {
            return Optional.empty();
        }

        return classA.equals(dominantClassification) ? a : b;
    }

    public Optional<ClassificationData> getArgClassification(ClassificationData... data) {
        return getArgClassification(Stream.of(data));
    }

    public Optional<ClassificationData> getArgClassification(Collection<ClassificationData> data) {
        return Optional.ofNullable(data)
                .map(x -> this.getArgClassification(x.stream()))
                .orElse(null);
    }

    public Optional<ClassificationData> getArgClassification(Stream<ClassificationData> data) {
        return Optional.ofNullable(data)
                .map(this::getArgReducedClassification)
                .orElse(null);
    }

    private static <T extends ClassificationData> Stream<ClassificationData> getStreamOfCollection(Collection<T> list) {
        return Optional.of(list).stream()
                .flatMap(actualList -> actualList.stream().map(item -> (ClassificationData) item));
    }

    public Optional<ClassificationData> getArgClassification(ClassificationDataHolder holder) {
        return Optional.ofNullable(holder)
                .map(actualHolder -> Stream.concat(
                        ClassificationMapper.getStreamOfCollection(holder.getCities()),
                        Stream.of(actualHolder.getGender(), actualHolder.getIncome())
                ))
                .map(this::getArgClassification)
                .filter(Optional::isPresent)
                .map(Optional::get);
    }
}
