package com.boko.jackson.poc;

import com.boko.jackson.poc.classifications.ClassificationData;
import com.boko.jackson.poc.classifications.ClassificationDataMixin;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Optional;

public class JsonSerde {
    private final ObjectMapper mapper;
    private final ObjectMapper polymorphicMapper;

    public JsonSerde() {
        this.mapper = new ObjectMapper()
                .findAndRegisterModules();

        this.polymorphicMapper = new ObjectMapper()
                .findAndRegisterModules()
                .addMixIn(ClassificationData.class, ClassificationDataMixin.class);
    }

    public <T> Optional<T> fromPolymorphicJson(final String json, final TypeReference<T> type) {
        try {
            return Optional.ofNullable(polymorphicMapper.reader().forType(type).readValue(json));
        }
        catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    public <T> Optional<T> fromJson(final String json, final Class<T> type) {
        try {
            return Optional.ofNullable(mapper.reader().forType(type).readValue(json));
        }
        catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    public <T> Optional<String> toPolymorphicJson(T object, final TypeReference<T> type) {
        try {
            return Optional.ofNullable(polymorphicMapper.writerWithDefaultPrettyPrinter().forType(type).writeValueAsString(object));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    public <T> Optional<String> toJson(T object) {
        try {
            return Optional.ofNullable(mapper.writerWithDefaultPrettyPrinter().forType(object.getClass()).writeValueAsString(object));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }
}
