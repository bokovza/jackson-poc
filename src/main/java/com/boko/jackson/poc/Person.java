package com.boko.jackson.poc;

import com.boko.jackson.poc.mapping.ClassificationDataHolder;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class Person {
    private String firstName;
    private String lastName;
    @JsonUnwrapped
    private ClassificationDataHolder holder;
}
